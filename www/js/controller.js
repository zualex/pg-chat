/**
 * Created by alexanderzulechner on 19.10.15.
 */

var app = angular.module('CPB_Chat', ['firebase']);


app.controller('CPB_Chat_Ctrl', ['$scope', '$log', 'Message', function ($scope, $log, Message) {

    $scope.messages = Message.all;
    $scope.localInfoFlag = false;


    /**
     * submit button
     * @param photo
     * @returns {boolean}
     */
    $scope.createMessage = function (photo, twitter) {
        var obj = new Object();
        //if message exists

        if (!$scope.newMessage && !photo) {
            alert("Nachricht eingeben!");
            return false;
        } else {
            if (photo) {
                obj.photo = photo;
            } else {
                if (twitter) {
                    window.plugins.socialsharing.shareViaTwitter($scope.newMessage);
                }
                obj.body = $scope.newMessage;
                $scope.newMessage = "";
            }
        }
        if (!$scope.person) {
            alert("Personenname eingeben!");
            return false;
        } else {
            obj.person = $scope.person;
        }
        if (!$scope.color) {
            alert("Farbe eingeben!");
        } else {
            obj.color = $scope.color;
        }
        //$scope.pushMessage(obj);
        Message.create(obj);
    };
    /**
     * creates message and push it to firebase
     */
    $scope.createPhotoMessage = function () {
        navigator.camera.getPicture(
            function (imageData) {
                $scope.createMessage(imageData);
            }, function (message) {
                alert('Failed because: ' + message);
            }, {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA, allowEdit: true,
                encodingType: Camera.EncodingType.JPEG, targetWidth: 600,
                targetHeight: 400,
                saveToPhotoAlbum: true
            }
        );
    };

    $scope.saveUserData = function () {
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem('color', $scope.color);
            localStorage.setItem('person', $scope.person);
        } else {
            informLocalStorage();
        }
    };

    $scope.getUserData = function () {
        if (typeof(Storage) !== "undefined") {
            $scope.person = localStorage.getItem("person");
            $scope.color = localStorage.getItem("color");
        } else {
            informLocalStorage();
        }

    };
    function informLocalStorage() {
        if (!$scope.storageInfoFlag) {
            alert("No local Storage Support!");
            $scope.storageInfoFlag = false;
        }
    }

    /**
     * localstorage
     * @returns {Array}
     */
    $scope.getMessages = function () {
        var messages = [];

        return messages;
    };

    /**
     *
     * @param position
     */
    function setLongLat(position) {

        $scope.lat = position.coords.latitude;
        $scope.long = position.coords.longitude;
        $scope.posFlag = true;
    }

    // One-shot position request.
    $scope.lat;
    $scope.long;
    $scope.posFlag = false;
    navigator.geolocation.getCurrentPosition(setLongLat);
    $scope.isChat = true;


    $scope.getUserData();


}]);

function scrollToBottom() {
    setTimeout(function () {
        window.scrollTo(0, document.body.scrollHeight);
    }, 100);
}

//factory 'Message' for read, write and delte entries in firebase
app.factory('Message', ['$firebase',
    function ($firebase) {
        var ref = new Firebase('https://torrid-torch-4859.firebaseio.com');
        var messages = $firebase(ref.child('messages')).$asArray();
        var Message = {
            all: messages,
            create: function (message) {
                return messages.$add(message);
            },
            get: function (messageId) {
                return $firebase(ref.child('messages').child(messageId)).$asObject();
            },
            delete: function (message) {
                return messages.$remove(message);
            }
        };
        ref.on('child_added', function (childSnapshot, prevChildKey) {
            scrollToBottom();
        });

        //vibrates if new message is sent
        ref.on("value", function (snapshot) {
            navigator.vibrate(1000);
            scrollToBottom();
        }, function (errorObject) {
            alert(errorObject.code);
        });

        return Message;
    }]);
